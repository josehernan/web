/*
Ajuste del ancho general y del ancho del banner
Tanto para verlo en PC como en Teléfono
*/

let contenedorGeneralPC = '85%'
let contenedorGeneralCEL = '100%'
let contenedorBannerPC = '55%'
let contenedorBannerCEL = '100%'

if(window.innerWidth >= 960) {
    $('.uk-container').css('max-width',contenedorGeneralPC)
    $('.block-carrousel__container.uk-container').css('max-width',contenedorBannerPC)
} else {
    $('.uk-container').css('max-width',contenedorGeneralCEL)
    $('.block-carrousel__container.uk-container').css('max-width',contenedorBannerCEL)
}
