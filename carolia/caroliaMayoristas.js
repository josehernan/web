$("#wholesaler-modal .modal__header.uk-modal-header p.modal__title.uk-modal-title.text--primary").text("Solicitar cuenta para Encuadernadores")
$("#wholesaler-modal .modal__header.uk-modal-header p.modal__subtitle.uk-text-justify").remove()
$("#wholesaler-modal div.modal__header.uk-modal-header").append("<p class='modal__subtitle uk-text-justify'>Completando este formulario solicitarás acceso a los diseños Carolia&#174; impresos e imprimibles. Chequeá las condiciones acá:</p>")
$("#wholesaler-modal div.modal__header.uk-modal-header").append("<a href='https://carolia.com.ar/sos-encuadernadora' target='_blank'>carolia.com.ar/sos-encuadernadora</a>")
$(".field__label--wholesaler_message").text('Página web donde subís tus trabajos')

const nuevoCampo = '<input name="wholesaler_message" id="wholesaler_message" class="field__textarea border-radius" data-regex="/^\bhttp:\/\/|\bhttps:\/\/|www.+[a-zA-Z0-9]|[a-zA-Z0-9].com$/" data-message="Ingrese una dirección válida" data-required="1">'

$("#wholesaler_message").replaceWith(nuevoCampo)
