$("body").append("<div class='bs-example'><div id='myModal' class='modal fade'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><h5 class='modal-title'>Términos y condiciones de los PDFs Carolia&#174;</h5><button type='button' class='close' data-dismiss='modal'>&times;</button></div><div class='modal-body'>")

$(".modal-body").append("<p>Todos los diseños Carolia&#174; tienen estrictas condiciones de uso. La compra y uso del archivo implica la aceptación de las siguientes condiciones:</p>")
$(".modal-body").append("<p>&#128683; Prohibida su reventa de forma digital.</p>")
$(".modal-body").append("<p>&#128683; Prohibido ceder el archivo a un tercero (el uso del archivo corresponde únicamente a la persona que lo abona y/o al emprendimiento/marca que tenga).</p>")
$(".modal-body").append("<p>&#9888;&#65039; El único uso pemitido del archivo es para imprimir, encuadernar (coser o anillar) y comercializar el cuaderno terminado.</p>")
$(".modal-body").append("<p>&#9888;&#65039; El diseño es exclusivo de la marca Carolia Encuadernación&#174;.</p>")
$(".modal-body").append("<p>&#128683; Prohibido utilizar alguna hoja del PDF como tapa del cuaderno, el uso es únicamente para el interior.</p>")
$(".modal-body").append("<p>&#128683; Prohibido editar/modificar de algún modo el archivo.</p>")
$(".modal-body").append("<p>&#128683; Prohibido agregar/ incluir elementos ajenos al diseño original.</p>")
$(".modal-body").append("<p>&#128683; Prohibido eliminar/tapar/quitar el logo Carolia&#174;.</p>")
$(".modal-body").append("<p>&#128683; Prohibido agregar/ incluir logos de terceros. (En caso de querer que se agregue tu logo, tiene un costo adicional).</p>")
$(".modal-body").append("<p>&#128683; Prohibido copiar/plagiar el diseño.</p>")
$(".modal-body").append("<p>&#9888;&#65039; Al ser ser un producto de tipo digital descargable Carolia® se reserva el derecho a no aceptar devoluciones ni solicitudes de reembolso.</p>")
$(".modal-body").append("<p>&#9888;&#65039; La calidad de la impresión dependerá 100% de la impresora (hogareña/de gráfica) que se utilice. Algunos elementos contenidos en el pdf pueden variar ligeramente (grosores/intensidades/colores).</p>")
$(".modal-body").append("<p>&#128073; El uso de diseños Carolia&#174; (únicamente impresión y encuadernado) implica el cumplimiento de las condiciones de uso. Cada impresión que se realice incluirá el logo CAROLIA y datos de contacto. &#128072;</p>")
$(".modal-body").append("<p>Si se lleva el archivo a una gráfica indicar que ellos no tienen permitido el uso comercial (imprimir y vender interiores por su cuenta). La venta de interiores impresos corresponde única y exclusivamente a Carolia&#174;.</p>")

$(".modal-body").append("<div class='form-check'><input class='form-check-input' type='checkbox' value='' id='check1'><span class='form-check-label' id='labelcheck1' for='check1'>No tengo gráfica ni revendo.</span></div>")
$(".modal-body").append("<div class='form-check'><input class='form-check-input' type='checkbox' value='' id='check2'><span class='form-check-label' id='labelcheck2' for='check2'>No me dedico a diseñar interiores de cuadernos/agendas.</span></div>")
$(".modal-body").append("<div class='form-check'><input class='form-check-input' type='checkbox' value='' id='check3'><span class='form-check-label' id='labelcheck3' for='check3'>Leí y Acepto los términos y condiciones de uso de los PDFs Carolia&#174;.</span></div>")

$(".modal-body").append("<br><p>La siguiente aceptación tiene valor de Contrato. Si se confirma el uso indebido de los archivos y/o el incumplimiento de los términos y condiciones, se dejará de vender nuevos archivos Carolia&#174;. En caso de continuar y/o reiterar el incumplimiento de los términos y condiciones, se iniciarán acciones legales.</p>")

$(".modal-content").append("</div><div class='modal-footer'><button id='aceptar-tyc' type='button' class='btn btn-secondary' disabled=''>De acuerdo</button></div></div></div></div></div>")

const insertarTYC=()=>{
setTimeout(()=>{
/* DESACTIVO BOTON ORIGINAL*/
$('#start_checkout-btn').attr('id','start_checkout-hack')
/* ACEPTAR DEL MODAL */
$('#aceptar-tyc').on('click',()=>{$._data($('.cart-sidenav__content')[0],'events').click.filter(e=>e.selector==="#start_checkout-btn")[0].handler()})

/* VINCULO NUEVA FUNCION */
$('.cart-sidenav__content').on('click','#start_checkout-hack',()=>{
if($('.cart-sidenav__product-title').text().toUpperCase().includes('PDF')){$('#myModal').modal({backdrop:'static'})}
else {$._data($('.cart-sidenav__content')[0],'events').click.filter(e=>e.selector==="#start_checkout-btn")[0].handler()}
})
},1000)
}

var origAppend=$.fn.append
$.fn.append=function(){return origAppend.apply(this, arguments).trigger("append")}
$("li").bind("append",()=>{insertarTYC()})

const verificarChecks=()=>{
if($('#check1').is(':checked')&&$('#check2').is(':checked')&&$('#check3').is(':checked')){
$('#aceptar-tyc').removeClass('btn-secondary')
$('#aceptar-tyc').addClass('btn-success')
$('#aceptar-tyc').removeAttr('disabled')
} else {
$('#aceptar-tyc').removeClass('btn-success')
$('#aceptar-tyc').addClass('btn-secondary')
$('#aceptar-tyc').attr('disabled','')
}}

$('#check1').on( 'click',()=>{verificarChecks()})
$('#check2').on( 'click',()=>{verificarChecks()})
$('#check3').on( 'click',()=>{verificarChecks()})
$('#labelcheck1').on('click',()=>{$('#check1').trigger('click')})
$('#labelcheck2').on('click',()=>{$('#check2').trigger('click')})
$('#labelcheck3').on('click',()=>{$('#check3').trigger('click')})
