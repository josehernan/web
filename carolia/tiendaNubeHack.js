var botonOriginal = $(document.getElementsByName('go_to_checkout')[0])
botonOriginal.parent().append('<a id="go_to_checkout_hack" href="#tyc-modal" data-toggle="modal" data-modal-url="modal-fullscreen-payments" class="js-fullscreen-modal-open js-product-payments-container visible-when-content-ready link-module product-payment-link"><input class="pull-right full-width-xs btn btn-primary" name="go_to_checkout_hack" data-component="cart.checkout-button" value="Iniciar Compra"></a>')
var botonHack = $('#go_to_checkout_hack')
botonHack.hide();

$("body").append('<div id="tyc-modal" class="js-fullscreen-modal js-mobile-installments-panel modal modal-xs modal-xs-right hide fade  two-gates " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>')

//$("#tyc-modal").append('<div class="modal-xs-dialog" style="max-height: 600px; overflow-y: auto;"><div class="modal-header with-tabs"><br /><h2>Términos y condiciones de los PDFs Carolia&#174;</h2><br /><div class="nav-tabs-container horizontal-container"><button type="button" class="btn-floating m-right-half m-top-quarter hidden-phone" data-dismiss="modal" aria-hidden="true"><svg class="svg-inline--fa" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"></path></svg></button></div></div><div id="tyc-items" class="modal-content">')
$("#tyc-modal").append('<div class="modal-xs-dialog" style="max-height: 600px; overflow-y: auto;"><div class="modal-header with-tabs"><br /><h2>Términos y condiciones de los PDFs Carolia&#174;</h2><br /><div class="nav-tabs-container horizontal-container"><button type="button" class="btn-floating m-right-half m-top-quarter hidden-phone" data-dismiss="modal" aria-hidden="true">x</button></div></div><div id="tyc-items" class="modal-content">')

//items
$("#tyc-items").append("Todos los diseños Carolia&#174; tienen estrictas condiciones de uso. La compra y uso del archivo implica la aceptación de las siguientes condiciones:")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibida su reventa de forma digital.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibido ceder el archivo a un tercero (el uso del archivo corresponde únicamente a la persona que lo abona y/o al emprendimiento/marca que tenga).</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#9888;&#65039; El único uso pemitido del archivo es para imprimir, encuadernar (coser o anillar) y comercializar el cuaderno terminado.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#9888;&#65039; El diseño es exclusivo de la marca Carolia Encuadernación&#174;.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibido utilizar alguna hoja del PDF como tapa del cuaderno, el uso es únicamente para el interior.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibido editar/modificar de algún modo el archivo.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibido agregar/ incluir elementos ajenos al diseño original.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibido eliminar/tapar/quitar el logo Carolia&#174;.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibido agregar/ incluir logos de terceros. (En caso de querer que se agregue tu logo, tiene un costo adicional).</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128683; Prohibido copiar/plagiar el diseño.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#9888;&#65039; Al ser ser un producto de tipo digital descargable Carolia® se reserva el derecho a no aceptar devoluciones ni solicitudes de reembolso.</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#9888;&#65039; La calidad de la impresión dependerá 100% de la impresora (hogareña/de gráfica) que se utilice. Algunos elementos contenidos en el pdf pueden variar ligeramente (grosores/intensidades/colores).</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>&#128073; El uso de diseños Carolia&#174; (únicamente impresión y encuadernado) implica el cumplimiento de las condiciones de uso. Cada impresión que se realice incluirá el logo CAROLIA y datos de contacto. &#128072;</p>")
$("#tyc-items").append("<p style='margin-top: 5px'>Si se lleva el archivo a una gráfica indicar que ellos no tienen permitido el uso comercial (imprimir y vender interiores por su cuenta). La venta de interiores impresos corresponde única y exclusivamente a Carolia&#174;.</p>")

//checks
$("#tyc-items").append("<br><label id='labelcheck1' class='checkbox-container' data-filter-name='labelcheck1' data-filter-value='labelcheck1' data-component='filter.option' data-component-value='No tengo gráfica ni revendo' style='margin-top: 5px; opacity: 0.5;'><input type='checkbox' autocomplete='off' id='check1'><span class='checkbox'><span class='checkbox-icon'></span>No tengo gráfica ni revendo.</span></label>")
$("#tyc-items").append("<label id='labelcheck2' class='checkbox-container' data-filter-name='labelcheck2' data-filter-value='labelcheck2' data-component='filter.option' data-component-value='No tengo gráfica ni revendo' style='margin-top: 5px; opacity: 0.5;'><input type='checkbox' autocomplete='off' id='check2'><span class='checkbox'><span class='checkbox-icon'></span>No me dedico a diseñar interiores de cuadernos/agendas.</span></label>")
$("#tyc-items").append("<label id='labelcheck3' class='checkbox-container' data-filter-name='labelcheck3' data-filter-value='labelcheck3' data-component='filter.option' data-component-value='No tengo gráfica ni revendo' style='margin-top: 5px; opacity: 0.5;'><input type='checkbox' autocomplete='off' id='check3'><span class='checkbox'><span class='checkbox-icon'></span>Leí y Acepto los términos y condiciones de uso de los PDFs Carolia&#174;.</span></label>")

$("#tyc-items").append("<br><p><i>La siguiente aceptación tiene valor de Contrato. Si se confirma el uso indebido de los archivos y/o el incumplimiento de los términos y condiciones, se dejará de vender nuevos archivos Carolia&#174;. En caso de continuar y/o reiterar el incumplimiento de los términos y condiciones, se iniciarán acciones legales.</i></p>")

$("#tyc-items").append("</div><div style='text-align: center;'><button id='aceptar-tyc' type='button' class='btn-secondary' disabled=''>De acuerdo</button><br><br></div>")

$("#tyc-modal").append("</div></div></div>")

const verificarChecks=()=>{
    if($('#check1').is(':checked')&&$('#check2').is(':checked')&&$('#check3').is(':checked')){
    $('#aceptar-tyc').removeClass('btn-secondary')
    $('#aceptar-tyc').addClass('btn-success')
    $('#aceptar-tyc').removeAttr('disabled')
    } else {
    $('#aceptar-tyc').removeClass('btn-success')
    $('#aceptar-tyc').addClass('btn-secondary')
    $('#aceptar-tyc').attr('disabled','')
    }
}

$('#check1').on( 'click',()=>{
    if($('#check1').is(':checked')) { $('#labelcheck1').css('opacity', '1') } else { $('#labelcheck1').css('opacity', '0.5') }
    verificarChecks();
})

$('#check2').on( 'click',()=>{
    if($('#check2').is(':checked')) { $('#labelcheck2').css('opacity', '1') } else { $('#labelcheck2').css('opacity', '0.5') }
    verificarChecks();
})

$('#check3').on( 'click',()=>{
    if($('#check3').is(':checked')) { $('#labelcheck3').css('opacity', '1') } else { $('#labelcheck3').css('opacity', '0.5') }
    verificarChecks();
})

$('#aceptar-tyc').on('click', ()=> { botonOriginal.click() })


const revisarHack = () => {
    if($("span[data-component*='name.short-name']").text().includes('PDF')) {
        botonOriginal.hide();
        botonHack.show(); 
    } else {
        botonHack.hide();
        botonOriginal.show();
    }
}

$('.js-addtocart').on('click', () => {  

setTimeout(() => {
    revisarHack()
  }, 2000);

})

$('#ajax-cart').on('click', () => { revisarHack() })

$('.js-toggle-cart').on('click', () => { revisarHack() })
