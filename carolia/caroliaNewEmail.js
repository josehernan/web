/*
Elimina el contenido original del contacto por email y lo reemplaza
*/

$('.fas.fa-envelope.footer__top__info-icon').parent().remove()
$('.footer__top__info-list.uk-flex.uk-flex-column').prepend('<li class="footer__top__info-item contrast_text--secondary"><a href="mailto:carolia@outlook.com" class="footer__top__info-link" target="_blank"><i class="fas fa-envelope footer__top__info-icon" aria-hidden="true"></i> carolia@outlook.com</a></li>')
