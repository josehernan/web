$('.desktop-list__item--cart').before('<li class="desktop-list__item contrast_text--primary"><a href="#contact-modal" class="desktop-list__link" uk-toggle="">Contacto</a></li>')

if($( "a[href*='#login-modal']" ).length != 0) {
    $('.desktop-list__item--cart').before('<li class="desktop-list__item contrast_text--primary"><a href="#wholesaler-modal" class="desktop-list__link" uk-toggle="">Acceso para Encuadernadores</a></li>')
    $('.desktop-list__item--cart').before('<li class="desktop-list__item contrast_text--primary"><a class="desktop-list__link">Cuenta<i class="desktop-list__down-icon contrast_text--primary fas fa-chevron-down" aria-hidden="true"></i></a><ul class="nav first background--primary"><li class="desktop-list__subitem contrast_text--primary"><a href="#register-modal" class="desktop-list__link" uk-toggle="">Crear cuenta</a></li><li class="desktop-list__subitem contrast_text--primary"><a href="#login-modal" class="desktop-list__link" uk-toggle="">Iniciar sesión</a></li></ul></li>')
}
else {
    $('.desktop-list__item--cart').before('<li class="desktop-list__item contrast_text--primary"><a href="/account" class="desktop-list__link">Mi cuenta</a></li>')
    $('.desktop-list__item--cart').before('<li class="desktop-list__item contrast_text--primary"><a href="/logout" class="desktop-list__link">Salir</a></li>')
}
