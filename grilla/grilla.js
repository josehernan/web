$(document).ready(function(){
	console.log('Inicio JQuery');
	
	//Manipulacion HTML
	document.title = "Grilla";
	
	//Definición del lado
	var d;
	if($(document).width() > $(document).height()) { d = $(document).height(); } else { d = $(document).width(); }
	d*=0.9;
	//console.log(d);

	var cantidadCeldas = 5;
	
	

//BORDE	
	$('<div id="grilla-borde"></div>').appendTo('body');
	$('#grilla-borde').css('width', d + 'px');
	$('#grilla-borde').css('height', d + 'px');
//

//INTERIOR
	$('<div id="grilla-interna"></div>').appendTo('#grilla-borde');
	$('#grilla-interna').css('width', 90 + '%');
	$('#grilla-interna').css('height', 90 + '%');
	$('#grilla-interna').css('margin', 5 + '%');
//

//ABAJO 
$('<div id="info" style="text-align:center; font-size:3em;"></div>').appendTo('body');
//

//FILAS
	var altoFilas = 0.9*100/cantidadCeldas;
	var marginTopFilas = 0.1*100/(cantidadCeldas - 1);
	var anchoFilas = 100/1;
	
	for( var i = 0 ; i < cantidadCeldas ; i++) 
		{ $('<div class="filas ' + 'fila_' + (i+1) + '"></div>').appendTo('#grilla-interna'); }

	$('.filas').each( function() {
		$(this).css('width', anchoFilas + '%');	$(this).css('height', altoFilas + '%');
		if( ! $(this).is(':first-child') ) { $(this).css('margin-top', marginTopFilas + '%'); }
	});
//

//CELDAS
	var altoCeldas = 100/1;
	var anchoCeldas = 0.9*100/cantidadCeldas;
	var leftCeldas = 0.1*100/(cantidadCeldas - 1);
	
	for( var i = 0 ; i < cantidadCeldas ; i++) 
		for( var j = 1 ; j <= cantidadCeldas ; j++) 
		{ $('<div class="celdas ' + 'columna_' + (i+1) + ' "></div>').appendTo('#grilla-interna .filas:nth-child(' + j + ')'); }
	
	$('.celdas').each( function() {
		$(this).css('width', anchoCeldas + '%'); $(this).css('height', altoCeldas + '%');
		if( ! $(this).is(':first-child') ) { $(this).css('margin-left', leftCeldas + '%'); }
	});
//	

//LLENAR
	for( var i = 1 ; i <= cantidadCeldas ; i++) 
		for( var j = 1 ; j <= cantidadCeldas ; j++) {
			$('<div class="ficha"></div>').appendTo('.filas:nth-child(' + i + ') .celdas:nth-child(' + j + ')');
		}
//

$('#grilla-interna').children(':last-child').children(':last-child').empty();

$('body').on('click', '.celdas', function() {
	var filaN = $(this).parent().attr('class').split('_');
	var columnaN = $(this).attr('class').split('_');
	
	$('#info').text('Fila ' + filaN[1] + ' - Columna ' + columnaN[1]);
	
	console.log('Fila ' + filaN[1] + ' - Columna ' + columnaN[1]);
});

$('body').on('mouseover', '.ficha', function() {
	$(this).css('background-color','#B71234');
});
  
$('body').on('mouseout', '.ficha', function() {
	$(this).css('background-color','#009900');
});

}); //ready jquery
